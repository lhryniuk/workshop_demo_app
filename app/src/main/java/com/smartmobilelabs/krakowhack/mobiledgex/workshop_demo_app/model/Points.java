package com.smartmobilelabs.krakowhack.mobiledgex.workshop_demo_app.model;

/**
 * this class represents the points coordinate on the screen
 */
public class Points {

    private float xCoord, yCoord, zCoord;

    private Points(float x, float y, float z){
        xCoord = x;
        yCoord = y;
        zCoord = z;
    }

    public Points(){
        this (0,0,0);
    }

    public float getxCoord() {
        return xCoord;
    }

    public void setxCoord(float xCoord) {
        this.xCoord = xCoord;
    }

    public float getyCoord() {
        return yCoord;
    }

    public void setyCoord(float yCoord) {
        this.yCoord = yCoord;
    }

    public float getzCoord() {
        return zCoord;
    }

    public void setzCoord(float zCoord) {
        this.zCoord = zCoord;
    }

}
