package com.smartmobilelabs.krakowhack.mobiledgex.workshop_demo_app;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.smartmobilelabs.evo.android.playersdk.player.Slot.OverlayViews.Mwc2019.Utils;
import com.smartmobilelabs.evo.android.playersdk.player.data.DataSmlTrackingObjectProtocol;
import com.smartmobilelabs.evo.android.playersdk.player.data.VideoViewDataOverlayBase;
import com.smartmobilelabs.evo.android.playersdk.player.util.EncodingToRTPLatencyMS;
import com.smartmobilelabs.krakowhack.mobiledgex.workshop_demo_app.model.Points;

import java.text.DecimalFormat;
import java.util.List;

import static com.smartmobilelabs.evo.android.playersdk.player.Slot.OverlayViews.Mwc2019.Utils.RoundedRect;



/**
 * <p> this is an example of an Overlay View.
 * it implements the ObjectPositionListener methods, to receive data such as object positions, metadata in json formats.
 * as an example, it demonstrate the object movement by drawing a red circle that follows the object.</>
 */
public class OverlayViewExample extends VideoViewDataOverlayBase implements DataSmlTrackingObjectProtocol.ObjectPositionListener{

    private static final String TAG = OverlayViewExample.class.getSimpleName();
    private static final int DELAY_REFRESH_SPEED = 30000;

    private DataSmlTrackingObjectProtocol mProto;
    private ImageButton display_overlay_btn;
    private Paint ballPaint;
    private TextPaint ballTextPaint;
    private TextPaint boxTextPaint;
    private Paint boxPaint;
    private Path pathDrawRect;
    private boolean continueToDraw;
    private int MAX_RADIUS = 200;
    private static int MIN_RADIUS = 70;
    private float emptyLeftSideWidth;
    private float emptyRightSideWidth;
    private Points currentPoint;
    private Points oldPoint;
    private EncodingToRTPLatencyMS encodeEncodingToRTPLatencyMS;
    private TextView highest_speed_txt;


    public OverlayViewExample(@NonNull Context context, Activity activity) {
        super(context);
        init(context, activity);
    }

    public OverlayViewExample(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public OverlayViewExample(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    void init(final Context context, Activity activity) {
        super.initialize(context);
        inflate(context, R.layout.overlay_example, this);

        pathDrawRect = new Path();

        // bind the views
        bindViews();

        // set listsners
        setViewsListeners();

        /* since the parent is FrameLayout which is a subclass of ViewGroup,
        we need to set willNotDraw to {false} so that the onDraw method will be called in runtime */
        setWillNotDraw(false);

        // set up the ball
        setUpTrackingBall();

        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(DELAY_REFRESH_SPEED);
                        activity.runOnUiThread(()
                                -> highest_speed_txt.setText("Highest speed: " + highestSpeed + " Km/h (refresh every 30s)"));
                    }
                } catch (InterruptedException e) {
                    Log.e(TAG, "Error: " + e.getMessage());
                }
            }
        };

        t.start();
    }


    /**
     * bind UI view elements
     */
    private void bindViews() {
        display_overlay_btn = findViewById(R.id.display_overlay_btn);
        highest_speed_txt = findViewById(R.id.highest_speed_txt);

        // ... other views
    }

    /**
     * add listeners to view elements
     */
    private void setViewsListeners() {
        display_overlay_btn.setOnClickListener( v -> {
            // TODO: create your own menu or views, to show metadata analytics ...
        });

        // ... other listeners
    }

    public synchronized void setEncodingToRTPLatency(EncodingToRTPLatencyMS newEncodeLatency) {
        encodeEncodingToRTPLatencyMS = newEncodeLatency;
    }

    /**
     * setup the ball canvas + text
     */
    private void setUpTrackingBall() {
        setupBallPaint();
        setUpBallText();

        setUpRect();
        setUpRectText();
    }

    /**
     *  Setup paint with color and stroke styles
     */
    private void setupBallPaint() {
        ballPaint = new Paint();
        ballPaint.setColor(getResources().getColor(android.R.color.holo_red_dark));
        ballPaint.setAntiAlias(true);
        ballPaint.setStrokeWidth(5);
        ballPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        ballPaint.setStrokeJoin(Paint.Join.ROUND);
        ballPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    /**
     *  Setup text
     */
    private void setUpBallText() {
        ballTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        ballTextPaint.setColor(getResources().getColor(com.smartmobilelabs.evo.android.playersdk.player.R.color.cardview_light_background));
        ballTextPaint.setTextSize(50);
        ballTextPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD_ITALIC));
        ballTextPaint.setTextAlign(Paint.Align.CENTER);
    }

    private void setUpRectText() {
        boxTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        boxTextPaint.setColor(getResources().getColor(R.color.cardview_light_background));
        boxTextPaint.setTextSize(50);
        boxTextPaint.setTextAlign(Paint.Align.CENTER);
    }

    private void setUpRect() {
        boxPaint = new Paint();
        boxPaint.setColor(getResources().getColor(android.R.color.holo_red_dark));
        boxPaint.setStrokeWidth(10);
        boxPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    }

    @Override
    protected DataSmlTrackingObjectProtocol createProtocolListener() {
        mProto = new  DataSmlTrackingObjectProtocol(null);
        mProto.addObjectPositionListener(this);
        return mProto;
    }

    @Override
    public void onObjectPositions(long l, List<DataSmlTrackingObjectProtocol.ObjectTrackingPosition> list) {
        if(list.isEmpty()) {
            // there is no data, stop drawing
            continueToDraw = false;
            invalidate();

        } else {
            // there is data, continue drawing
            continueToDraw = true;
            // update the position of the ball
            updateCurrentPosition(list.get(0).x, list.get(0).y, list.get(0).z_size);
        }
    }

    /**
     *
     * @param l time stamp
     * @param list the list of objects received
     */
    @Override
    public void onObjectMetadata(long l, List<DataSmlTrackingObjectProtocol.ObjectMetaData> list) {
        if (list != null && list.size() != 0) {
            DataSmlTrackingObjectProtocol.ObjectMetaData tmp = list.get(0);
            String jsonString = tmp.metadata; // the metadata is in json format

            // TODO: do amazing things with the metadata
        }
    }

    /**
     * this needs to be overrated since the protocol needs the rendered view width and height
     * @param vRenderedWidth rendered width
     * @param vRenderedHeight rendered height
     */
    @Override
    public void videoRendererListener(int vRenderedWidth, int vRenderedHeight) {
        if (mProto != null) {
            Log.d("wkh", "video width: " +  vRenderedWidth);
            mProto.setVideoHeight(vRenderedHeight);
            mProto.setVideoWidth(vRenderedWidth);
            mProto.setEncodeLatencyValue(encodeEncodingToRTPLatencyMS);
            calculateNoRenderedView(vRenderedWidth);
        }
    }

    private void calculateNoRenderedView(int w) {
        int[] location = new int[2];
        getLocationOnScreen(location);
        int x = location[0];
        int y = location[1];
        emptyLeftSideWidth = Utils.calculateLeftSideNoRenderSizePixels(location, getContext(), w);
        emptyRightSideWidth = emptyLeftSideWidth + w;
    }


    private void updateCurrentPosition(int x_position, int y_position, float z_position) {

        if(currentPoint == null) {
            currentPoint = new Points();
        }
        if( oldPoint == null) {
            oldPoint = new Points();
        }

        if(!oldPoint.equals(currentPoint)) {
            // save values
            oldPoint.setxCoord(currentPoint.getxCoord());
            oldPoint.setyCoord(currentPoint.getyCoord());
            oldPoint.setzCoord(currentPoint.getzCoord());
        }

        // set new values
        currentPoint.setxCoord(x_position);
        currentPoint.setyCoord(y_position);
        currentPoint.setzCoord(z_position);

        invalidate();

    }

    public double distanceTo(Points p) {
        return Math.sqrt(Math.pow(oldPoint.getxCoord() - p.getxCoord(), 2)
                + Math.pow(oldPoint.getyCoord() - p.getyCoord(), 2)
                + Math.pow(oldPoint.getzCoord() - p.getzCoord(), 2));
    }

    public int getHours() {
        long milliseconds = System.currentTimeMillis();
        return (int) ((milliseconds / (1000*60*60)) % 24);
    }

    /**
     *  approximate calculation of speed of the ball
     * @return String speed
     */
    private String calculateSpeed() {
        return new DecimalFormat("##").format(distanceTo(currentPoint) / getHours());
    }

    int highestSpeed = 0;
    private void getHighestSpeed() {
        if(highestSpeed < Integer.valueOf(calculateSpeed())) {
            highestSpeed = Integer.valueOf(calculateSpeed());
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(continueToDraw) {
            if(emptyLeftSideWidth != 0 && emptyRightSideWidth != 0) {
                if( !(currentPoint.getxCoord() == 0)
                        && !(currentPoint.getyCoord() == 0)
                        && (currentPoint.getxCoord() + emptyLeftSideWidth)
                        > (emptyLeftSideWidth + (currentPoint.getzCoord()*1.25))
                        && (currentPoint.getxCoord() + emptyLeftSideWidth)
                        < (emptyRightSideWidth - (currentPoint.getzCoord()*1.25))) {

                    if(currentPoint.getzCoord() < MIN_RADIUS) {
                        currentPoint.setzCoord(MIN_RADIUS);
                    } else if(currentPoint.getzCoord() > MAX_RADIUS) {
                        currentPoint.setzCoord(MAX_RADIUS);
                    }

                    canvas.drawCircle(currentPoint.getxCoord() + emptyLeftSideWidth,
                            currentPoint.getyCoord(),
                            currentPoint.getzCoord(), // radius
                            ballPaint);
                    canvas.drawText(""+calculateSpeed(),
                            currentPoint.getxCoord() + emptyLeftSideWidth,
                            currentPoint.getyCoord(),
                            ballTextPaint);
                    pathDrawRect = RoundedRect(currentPoint.getxCoord() + 30,
                            currentPoint.getyCoord() - 180,
                            currentPoint.getxCoord() + 430,
                            currentPoint.getyCoord() + - 80 ,
                            50,
                            50,
                            true,
                            true,
                            true,
                            true);
                    canvas.drawPath(pathDrawRect, boxPaint);
                    canvas.drawText("Speed (km/h)",
                            currentPoint.getxCoord() + 220,
                            currentPoint.getyCoord() - 110,
                            boxTextPaint);
                    canvas.setDensity(DisplayMetrics.DENSITY_MEDIUM); // for low density screens
                }
            }
            getHighestSpeed();
        }
    }
}
