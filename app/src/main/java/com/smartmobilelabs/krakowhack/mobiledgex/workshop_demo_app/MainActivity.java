package com.smartmobilelabs.krakowhack.mobiledgex.workshop_demo_app;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

/**
 * this activity shows how to integrate the sdk
 * it implements the MultiViewActivityInterface used by the multiview fragment
 */
public class MainActivity extends AppCompatActivity {

    // TODO: declare the MultiView variable


    // TODO: declare the CameraView variable


    // cardView camera/multiview container
     private CardView camera_card_view;
     private CardView multiview_card_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestPermissionsIfNeeded();
        
        camera_card_view = findViewById(R.id.camera_card_view);
        multiview_card_view = findViewById(R.id.multiview_card_view);

        // TODO: bind the MultiView fragment

        // TODO: bind the CameraView fragment

        // TODO: pass this Activity and the factory to the MultiView

    }

    @Override
    protected void onStart() {
        super.onStart();

        // TODO: Copy/paste the code of the setup configuration for the MultiView

        // TODO: Copy/paste the code of the setup configuration for the CameraFragment

    }

    @Override
    public void onBackPressed() {
        // TODO: avoid to quit the application from the fullscreen mode when back button is pressed

    }

    private void requestPermissionsIfNeeded() {
        if (!checkPermissions()) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.INTERNET,
                            Manifest.permission.CAMERA,
                            Manifest.permission.RECORD_AUDIO
                    }, 1);
        }
    }

    private boolean checkPermissions() {
        boolean isPermission = true;

        int result;
        result = checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result != PackageManager.PERMISSION_GRANTED) {
            isPermission = false;
        }
        result = checkCallingOrSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result != PackageManager.PERMISSION_GRANTED) {
            isPermission = false;
        }
        result = checkCallingOrSelfPermission(Manifest.permission.INTERNET);
        if (result != PackageManager.PERMISSION_GRANTED) {
            isPermission = false;
        }
        result = checkCallingOrSelfPermission(Manifest.permission.CAMERA);
        if (result != PackageManager.PERMISSION_GRANTED) {
            isPermission = false;
        }
        result = checkCallingOrSelfPermission(Manifest.permission.RECORD_AUDIO);
        if (result != PackageManager.PERMISSION_GRANTED) {
            isPermission = false;
        }
        return isPermission;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        // If request is cancelled, the result arrays are empty.
        if (requestCode == 1) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED
                    && grantResults[4] == PackageManager.PERMISSION_GRANTED) {
                // permissions are granted
            }
        }
    }

}
